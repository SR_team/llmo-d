module llmo.hook.simple;

private import llmo.mem, llmo.hook;

public:
/// Simple jump hook
ubyte[] simpleJmpHook(size_t addr, void* func, ssize_t length = 5){
	return simpleJmpHook(cast(void*)addr, func, length);
}
/// Simple jump hook
ubyte[] simpleJmpHook(void* addr, void* func, ssize_t length = 5){
	ubyte[] original = readMemory(addr, length);
	if (!original.length) return original;
	setMemory(addr, cast(ubyte)0x90, length, MemorySafe.prot);
	writeMemory!ubyte(cast(ubyte*)addr, cast(ubyte)0xE9, MemorySafe.prot);
	writeMemory(cast(size_t)addr + 1, calcRelAddr(addr, func), MemorySafe.prot);
	return original;
}

/// Simple call hook
ubyte[] simpleCallHook(size_t addr, void* func, ssize_t length = 5){
	return simpleCallHook(cast(void*)addr, func, length);
}
/// Simple call hook
ubyte[] simpleCallHook(void* addr, void* func, ssize_t length = 5){
	ubyte[] original = readMemory(addr, length);
	if (!original.length) return original;
	setMemory(addr, cast(ubyte)0x90, length, MemorySafe.prot);
	writeMemory!ubyte(cast(ubyte*)addr, cast(ubyte)0xE8, MemorySafe.prot);
	writeMemory(cast(size_t)addr + 1, calcRelAddr(addr, func), MemorySafe.prot);
	return original;
}