module llmo.hook.shortasm;

private import llmo.hook;
import std.conv : to;

public:
/// Return code for save registers
ubyte[] saveRegisters(){
	ubyte[] result;
	version(X86){
		result ~= cast(ubyte)0x60;
		result ~= cast(ubyte)0x9C;
	}
	version(X86_64){
		static foreach (key; 0x50 .. 0x58)
			static if (key != 0x54)
				result ~= cast(ubyte)key;
		static foreach (key; 0x50 .. 0x58)
			result ~= [0x41, key].to!(ubyte[]);
	}
	return result;
}
/// Return code for save x87 registers (FPU, MMX, XMM, and MXCSR)
ubyte[] savex87Registers(ref ubyte[512] fpu){
	ubyte[] result;
	version(X86){
		result ~= [0x0F, 0xAE, 0x05].to!(ubyte[]) ~ toUbyteArray(fpu.ptr);
	}
	version(X86_64){
		result ~= [0x50, 0x48, 0xB8].to!(ubyte[]);
		result ~= toUbyteArray(fpu.ptr);
		result ~= [0x0F, 0xAE, 0x00, 0x58].to!(ubyte[]);
	}
	return result;
}

/// Return code for load registers
ubyte[] loadRegisters(){
	ubyte[] result;
	version(X86){
		result ~= cast(ubyte)0x9D;
		result ~= cast(ubyte)0x61;
	}
	version(X86_64){
		static foreach (key; 0 .. 8)
			static if (key != 4)
				result ~= [0x41, 0x5F - key].to!(ubyte[]);
		static foreach (key; 0 .. 8)
			static if (key != 4)
				result ~= cast(ubyte)(0x5F - key);
	}
	return result;
}
/// Return code for load x87 registers (FPU, MMX, XMM, and MXCSR)
ubyte[] loadx87Registers(ref ubyte[512] fpu){
	ubyte[] result;
	version(X86){
		result ~= [0x0F, 0xAE, 0x0D].to!(ubyte[]) ~ toUbyteArray(fpu.ptr);
	}
	version(X86_64){
		result ~= [0x50, 0x48, 0xB8].to!(ubyte[]);
		result ~= toUbyteArray(fpu.ptr);
		result ~= [0x0F, 0xAE, 0x08, 0x58].to!(ubyte[]);
	}
	return result;
}

/// Return code for jump to 0x00000000. You need paste address manualy (opcode addr + 1)
ubyte[] jumpToNull(){
	return [0xE9, 0x00, 0x00, 0x00, 0x00].to!(ubyte[]);
}

/**
 * Return code for jump to 0x00000000. You need paste address manualy
 * X86: paste relative address at opcode addr + 1
 * X86_64: paste physic address at opcode addr + 3
 */
ubyte[] callToNull(){
	version(X86){
		return [0xE8, 0x00, 0x00, 0x00, 0x00].to!(ubyte[]);
	}
	version(X86_64){
		ubyte[] result = [0x50, 0x48, 0xB8].to!(ubyte[]);
		result ~= [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00].to!(ubyte[]);
		result ~= [0xFF, 0xD0, 0x58].to!(ubyte[]);
		return result;
	}
}

/**
 * Fix null pointer for callToNull and jumpToNull
 * DO NOT CALL THIS FUNCTION WITH D DYNAMIC ARRAY!!! Use static array (e.g. ubyte[10] arr;) or manualy allocated memory (e.g. core.memmory.pureMalloc)
 */
bool fixNull(ubyte* addr, void*func){
	version(X86){
	if (addr[0] != 0xE9 && addr[0] != 0xE8)
		return false;
	*cast(size_t*)(cast(size_t)addr + 1) = cast(size_t)calcRelAddr(addr, func);
	return true;
	}
	version(X86_64){
		immutable dist = calcRelAddr(addr, func);
		if (addr[0] == 0xE9 && (cast(long)dist < 0x80000000 || cast(long)dist > 0x7FFFFFFF))
			return false;
		else if (addr[0] == 0xE9)
			*cast(uint*)(cast(size_t)addr + 1) = cast(uint)calcRelAddr(addr, func);
		else
			*cast(size_t*)(cast(size_t)addr + 3) = cast(size_t)func;
		return true;
	}
}

/// Return from called function
ubyte[] callReturn(ushort pops = 0){
	if (!pops)
		return [0xC3].to!(ubyte[]);
	else {
		ubyte[] result = [0xC2, 0x00, 0x00].to!(ubyte[]);
		*cast(ushort*)(cast(size_t)result.ptr + 1) = pops;
		return result;
	}
}

/// Push value to stack
ubyte[] push(uint value){
	return cast(ubyte)0x68 ~ toUbyteArray(value);
}

/// Return code for set context (ECX/RCX)
ubyte[] setContext(void* ctx){
	version(X86){
		return [0xB9].to!(ubyte[]) ~ toUbyteArray(ctx);
	}
	version(X86_64){
		return [0x48, 0xB9].to!(ubyte[]) ~ toUbyteArray(ctx);
	}
}

/// Return code for get context (ECX/RCX)
ubyte[] getContext(void* ctx){
	version(X86){
		return [0x89, 0x0D].to!(ubyte[]) ~ toUbyteArray(ctx);
	}
	version(X86_64){
		ubyte[] result = [0x50, 0x48, 0xB8].to!(ubyte[]);
		result ~= toUbyteArray(ctx);
		result ~= [0x48, 0x89, 0x08, 0x58].to!(ubyte[]);
		return result;
	}
}

/// Return code to push register to stack
ubyte[] pushRegister(RegisterId id){
	size_t rid = cast(size_t)id;
	if (rid < 8) return [0x50 + rid].to!(ubyte[]);
	version(X86_64){
		return [0x41, 0x50 + (rid - 8)].to!(ubyte[]);
	}
	version(X86){ // Stupid compiler
		ubyte[] dummy;
		return dummy;
	}
}

/// Return code to pop register from stack
ubyte[] popRegister(RegisterId id){
	size_t rid = cast(size_t)id;
	if (rid < 8) return [0x58 + rid].to!(ubyte[]);
	version(X86_64){
		return [0x41, 0x58 + (rid - 8)].to!(ubyte[]);
	}
	version(X86){ // Stupid compiler
		ubyte[] dummy;
		return dummy;
	}
}

version(X86){
	/// Return code for copy registers to store
	ubyte[] copyFromRegisters(size_t[8] store){
		return copyFromRegisters(cast(size_t)store.ptr);
	}
	/// Return code for copy store to registers
	ubyte[] copyToRegisters(size_t[8] store){
		return copyToRegisters(cast(size_t)store.ptr);
	}
}
version(X86_64){
	/// Return code for copy registers to store
	ubyte[] copyFromRegisters(size_t[16] store){
		return copyFromRegisters(cast(size_t)store.ptr);
	}
	/// Return code for copy store to registers
	ubyte[] copyToRegisters(size_t[16] store){
		return copyToRegisters(cast(size_t)store.ptr);
	}
}
/// Return code for copy registers to store
ubyte[] copyFromRegisters(size_t ptr /* size 32 - 128 bytes */){
	ubyte[] result;
	version(X86){
		result ~= cast(ubyte)0xA3 ~ toUbyteArray(ptr);
		static foreach (ubyte key; 1 .. 8)
			result ~= [0x89, 0x05 + key * 8].to!(ubyte[]) ~ toUbyteArray(ptr + key * 4);
	}
	version(X86_64){
		result ~= [0x50, 0x48, 0x83, 0xC4, 0x08, 0x48, 0xA3].to!(ubyte[]);
		result ~= toUbyteArray(ptr);
		static foreach (ubyte key; 1 .. 8){
			result ~= [0x48, 0x89, 0xC0 + key * 8].to!(ubyte[]);
			result ~= [0x48, 0xA3].to!(ubyte[]) ~ toUbyteArray(ptr + key * 8);
		}
		static foreach (ubyte key; 0 .. 8){
			result ~= [0x4C, 0x89, 0xC0 + key * 8].to!(ubyte[]);
			result ~= [0x48, 0xA3].to!(ubyte[]) ~ toUbyteArray(ptr + key * 8 + 64);
		}
		result ~= [0x48, 0x83, 0xEC, 0x08, 0x58].to!(ubyte[]);
	}
	return result;
}
/// Return code for copy store to registers
ubyte[] copyToRegisters(size_t ptr /* size 32 - 128 bytes */){
	ubyte[] result;
	version(X86){
		result ~= cast(ubyte)0xA1 ~ toUbyteArray(ptr);
		static foreach (ubyte key; 1 .. 8)
			result ~= [0x8B, 0x05 + key * 8].to!(ubyte[]) ~ toUbyteArray(ptr + key * 4);
	}
	version(X86_64){
		result ~= [0x50, 0x48, 0x83, 0xC4, 0x08, 0x48, 0xA1].to!(ubyte[]);
		result ~= toUbyteArray(ptr);
		static foreach (ubyte key; 1 .. 8){
			result ~= [0x48, 0xA3].to!(ubyte[]) ~ toUbyteArray(ptr + key * 8);
			result ~= [0x48, 0x89, 0xC0 + key].to!(ubyte[]);
		}
		static foreach (ubyte key; 0 .. 8){
			result ~= [0x48, 0xA3].to!(ubyte[]) ~ toUbyteArray(ptr + key * 8 + 64);
			result ~= [0x49, 0x89, 0xC0 + key].to!(ubyte[]);
		}
		result ~= [0x48, 0x83, 0xEC, 0x08, 0x58].to!(ubyte[]);
	}
	return result;
}

/// Return code for modify stack pointer at passed delta
ubyte[] modifyStack(byte at){
	if (!at){
		ubyte[] dummy;
		return dummy;
	}
	if (at){
		version(X86){
			return [0x83, 0xC4, at].to!(ubyte[]);
		}
		version(X86_64){
			return [0x48, 0x83, 0xC4, at].to!(ubyte[]);
		}
	} else {
		version(X86){
			return [0x83, 0xEC, -cast(int)at].to!(ubyte[]);
		}
		version(X86_64){
			return [0x48, 0x83, 0xEC, -cast(int)at].to!(ubyte[]);
		}
	}
}

version(X86){
	/// Id of X86 register
	enum RegisterId{
		EAX = 0,
		ECX,
		EDX,
		EBX,
		ESP,
		EBP,
		ESI,
		EDI
	}
}
version(X86_64){
	/// Id of X86_64 register
	enum RegisterId{
		RAX = 0,
		RCX,
		RDX,
		RBX,
		RSP,
		RBP,
		RSI,
		RDI,
		R8,
		R9,
		R10,
		R11,
		R12,
		R13,
		R14,
		R15
	}
}

package:
ubyte[T.sizeof] toUbyteArray(T)(T v) @nogc pure nothrow{
	union converter{
		T v;
		ubyte[T.sizeof] b;
	}
	converter c;
	c.v = v;
	return c.b;
}