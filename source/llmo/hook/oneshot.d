module llmo.hook.oneshot;

private import llmo.mem, llmo.hook;
import core.memory : pureMalloc;
import std.conv : to;

public:
alias OneShotFunc_t = extern(Windows) void function();

/**
 * Install hook, that called once\
 * You need call `pureFree` from `core.memory` for returned pointer, when hook is called
 */
void* installOneShotHook(size_t address, OneShotFunc_t func){
	return installOneShotHook(cast(void*)address, func);
}
/**
 * Install hook, that called once\
 * You need call `pureFree` from `core.memory` for returned pointer, when hook is called
 */
void* installOneShotHook(void* address, OneShotFunc_t func){
	void* originalCode = pureMalloc(4096);
	void* code = cast(void*)(cast(size_t)originalCode + 5);

	ubyte[] builder = saveRegisters();
	immutable callFunc = builder.length; // Offset to call code
	builder ~= callToNull(); // dummy call for calling passed function
	static foreach(i; 0 .. 5){ // Restore original code
		version(X86) builder ~= cast(ubyte)0xA1 ~ toUbyteArray(cast(size_t)originalCode + i);
		version(X86_64) builder ~= [0x48, 0xA1].to!(ubyte[]) ~ toUbyteArray(cast(size_t)originalCode + i);
		builder ~= cast(ubyte)0xA2 ~ toUbyteArray(cast(size_t)address + i);
	}
	builder ~= loadRegisters();
	builder ~= jumpToNull(); // dummy jump for return to original code
	// Jump to original code
	writeMemory(code, builder, MemorySafe.unsafe);
	auto jmpAddr = cast(size_t)code + builder.length - 4;
	writeMemory(jmpAddr, calcRelAddr(jmpAddr - 1, address), MemorySafe.unsafe);
	// Call passed function
	auto callAddr = cast(size_t)code + callFunc;
	version(X86) writeMemory(callAddr + 1, calcRelAddr(callAddr, &func), MemorySafe.unsafe);
	version(X86_64) writeMemory(callAddr + 3, &func, MemorySafe.unsafe);
	// Jump to hook
	auto original = simpleJmpHook(address, code);
	// Backup original code
	writeMemory(originalCode, original, MemorySafe.unsafe);
	// Set all access to hook code
	setMemoryProtection(code, builder.length);
	return originalCode;
}