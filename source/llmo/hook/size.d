module llmo.hook.size;

private import llmo.mem, llmo.hook;
/*
public:
/// Read asm code and calculate minimal size for hook | O(N^2)
uint calcHookSize(size_t addr, ssize_t maxSize = 0x14){
	return calcHookSize(cast(void*)addr, maxSize);
}
/// Read asm code and calculate minimal size for hook | O(N^2)
uint calcHookSize(void* addr, ssize_t maxSize = 0x14){
	if (maxSize < 5)
		return 0;
	auto code = readMemory(addr, maxSize, MemorySafe.safe);
	if (!code.length)
		return 0;
	version(X86){
		auto cs = create(Arch.x86, ModeFlags(Mode.bit32));
	}
	version(X86_64){
		auto cs = create(Arch.x86, ModeFlags(Mode.bit64));
	}
	for(uint i = 5; i < maxSize; i++){
		auto res = cs.disasm(code[0 .. i], 0x10000000);
		uint length = 0;
		foreach(instr; res)
			length += instr.bytes.length;
		if (length >= 5)
			return length;
	}
	return 0;
}

unittest{
	auto CODE = cast(ubyte[])"\x55\x54\xE9\x11\x22\x33\x44\xE9\x11\x22\x33\x44\xE9\x11\x22\x33\x44\xE9\x11\x22\x33\x44";
	assert(calcHookSize(cast(void*)CODE.ptr, CODE.length) == 7);
}*/
