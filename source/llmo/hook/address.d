module llmo.hook.address;

private import llmo.hook;

public:
/// Calculate relative address from memory address to function
size_t calcRelAddr(size_t addr, void* func){
	return cast(size_t)calcRelAddr(cast(void*)addr, func);
}
/// Calculate relative address from memory address to function
void* calcRelAddr(void* addr, void* func){
	return cast(void*)(cast(size_t)func - (cast(size_t)addr + 5));
}

/// Calculate physical address of function by memory address and relative address
size_t calcPhysAddr(size_t addr, size_t relAddr){
	return cast(size_t)calcPhysAddr(cast(void*)addr, cast(void*)relAddr);
}
/// Calculate physical address of function by memory address and relative address
void* calcPhysAddr(void* addr, void* relAddr){
	return cast(void*)(cast(size_t)relAddr + (cast(size_t)addr + 5));
}