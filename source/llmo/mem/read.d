module llmo.mem.read;
private import llmo.mem;
import core.stdc.string;

public:
/// Reading value from address
T readMemory(T)(size_t addr, MemorySafe safe = MemorySafe.valid){
	return readMemory(cast(T*)addr, safe);
}
/// Reading value from address
T readMemory(T)(T* addr, MemorySafe safe = MemorySafe.valid){
	T result;
	if (safe == MemorySafe.valid || safe == MemorySafe.safe)
		if (!testMemoryAccess(addr))
			return result;
	uint prot;
	if (safe == MemorySafe.prot || safe == MemorySafe.safe){
		prot = getMemoryProtection(addr);
		setMemoryProtection(addr, T.sizeof, 0x40);
	}
	result = *addr;
	if (safe == MemorySafe.prot || safe == MemorySafe.safe)
		setMemoryProtection(addr, T.sizeof, prot);
	return result;
}

/// Reading array from address
ubyte[] readMemory(size_t addr, ssize_t length, MemorySafe safe = MemorySafe.valid){
	return readMemory(cast(void*)addr, length, safe);
}
/// Reading array from address
ubyte[] readMemory(void* addr, ssize_t length, MemorySafe safe = MemorySafe.valid){
	ubyte[] result;
	if (safe == MemorySafe.valid || safe == MemorySafe.safe)
		if (!testMemoryAccess(addr))
			return result;
	result.length = length;
	uint prot;
	if (safe == MemorySafe.prot || safe == MemorySafe.safe){
		prot = getMemoryProtection(addr);
		setMemoryProtection(addr, length, 0x40);
	}
	memcpy(result.ptr, addr, length);
	if (safe == MemorySafe.prot || safe == MemorySafe.safe)
		setMemoryProtection(addr, length, prot);
	return result;
}