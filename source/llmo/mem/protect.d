module llmo.mem.protect;

private import llmo.mem;
version (Posix){
	import core.stdc.stdlib;
	import std.file;
	import std.string;
	import std.regex;
}

public:
/// Return protection of page
uint getMemoryProtection(size_t addr){
	return getMemoryProtection(cast(void*)addr);
}
uint getMemoryProtection(void* addr){
	version(Windows){
		MEMORY_BASIC_INFORMATION mbi;
		VirtualQuery(addr, &mbi, mbi.sizeof);
		return mbi.Protect;
	}
	version (Posix){
		enum re = ctRegex!(`([0-9a-f]+)-([0-9a-f]+)\s+([r-][w-][x-]).*`);
		string[] maps = "/proc/self/maps".readText.splitLines;
		foreach(map; maps){
			auto rm = matchFirst(map, re);
			if (rm.empty) continue;
			const(ulong) start = strtoull(rm[1].toStringz, null, 16);
			const(ulong) end = strtoull(rm[2].toStringz, null, 16);
			if (start <= cast(size_t)addr && end >= cast(size_t)addr){
				if (rm[3] == "rwx") return 0x40;
				if (rm[3] == "r-x") return 0x20;
				if (rm[3] == "--x") return 0x10;
				if (rm[3] == "rw-") return 0x04;
				if (rm[3] == "-wx") return 0x41;
				if (rm[3] == "r--") return 0x02;
				if (rm[3] == "-w-") return 0x08;
				return 0; // Disallow all, but allocated
			}
		}
		return 1;
	}
}

/// Set new protection
bool setMemoryProtection(immutable(size_t) addr, ssize_t size, uint prot = 0x40){
	return setMemoryProtection(cast(void*)addr, size, prot);
}
bool setMemoryProtection(void* addr, ssize_t size, uint prot = 0x40){
	version(Windows){
		do
		{
			MEMORY_BASIC_INFORMATION mbi;
			if (!VirtualQuery(addr, &mbi, mbi.sizeof))
				return false;
			if (size > mbi.RegionSize)
				size -= mbi.RegionSize;
			else
				size = 0;
			uint vp;
			if (!VirtualProtect(mbi.BaseAddress, mbi.RegionSize, prot, &vp))
				return false;
			if (cast(size_t)mbi.BaseAddress + mbi.RegionSize < cast(size_t)addr + size)
				addr = cast(void*)(cast(size_t)mbi.BaseAddress + mbi.RegionSize);
		} while (size);
		return true;
	}
	version (Posix){
		if (prot >= 0x40)
			prot = PROT_EXEC | PROT_WRITE | PROT_READ;
		else if (prot == 0x20)
			prot = PROT_EXEC | PROT_READ;
		else if (prot == 0x10)
			prot = PROT_EXEC;
		else if (prot == 0x02)
			prot = PROT_READ;
		else if (prot == 0x04 || prot == 0x08)
			prot = PROT_WRITE | PROT_READ;
		else return false;
		return mprotect(addr, size, prot) == 0;
	}
}

/// Return true, if memory accesable
bool testMemoryAccess(immutable(size_t) addr){
	return testMemoryAccess(cast(void*)addr);
}
/// Return true, if memory accesable
bool testMemoryAccess(void* addr){
	version (Windows){
		__gshared SYSTEM_INFO si;
		if (si.lpMinimumApplicationAddress == null)
			GetSystemInfo(&si);
		if (addr < si.lpMinimumApplicationAddress)
			return false;
		if (addr > si.lpMaximumApplicationAddress)
			return false;
	}
	if (getMemoryProtection(addr) == 0x01)
		return false;
	return true;
}

/// safe level for memory operations
enum MemorySafe{
	unsafe, /// no any checks and changes
	valid, /// check only pointer is valid
	prot, /// change permissions, no check pointer
	safe /// check pointer and change permissions
}

unittest{
	alias func_t = extern(C) int function() @nogc nothrow pure @safe;
	version(X86){
		// Execute code from readonly section
		enum code = "\xb8\x20\x00\x00\x00\xc3";
		void* code_ptr = cast(void*)code.ptr;
		setMemoryProtection(code_ptr, code.length, 0x40);
		func_t f = cast(func_t)code_ptr;
		assert(f() == 32);

		// check access to invalid pointer
		assert(!testMemoryAccess(0xFFFFFFFF));
	}
	version(X86_64){
		// Execute code from readonly section
		enum code = "\x48\xc7\xc0\x40\x00\x00\x00\xc3";
		void* code_ptr = cast(void*)code.ptr;
		setMemoryProtection(code_ptr, code.length, 0x40);
		func_t f = cast(func_t)code_ptr;
		assert(f() == 64);

		// check access to invalid pointer
		assert(!testMemoryAccess(0xFFFFFFFFFFFFFFFF));
	}
}