module llmo.mem.set;

private import llmo.mem;
import core.stdc.string;

public:
int setMemory(size_t addr, ubyte value, ssize_t length, MemorySafe safe = MemorySafe.valid){
	return setMemory(cast(void*)addr, value, length, safe);
}
int setMemory(void* addr, ubyte value, ssize_t length, MemorySafe safe = MemorySafe.valid){
	ubyte* _addr = cast(ubyte*)addr;
	ubyte[] buf;
	buf.length = length > 4096 ? 4096 : length;
	buf[] = value;
	while (true){
		if (length > 4096){
			if (!writeMemory(cast(void*)_addr, buf, safe))
				return 0;
			_addr += 4096;
			length -= 4096;
		} else {
			buf.length = length;
			if (!writeMemory(cast(void*)_addr, buf, safe))
				return 0;
			break;
		}
	}
	return 1;
}