module llmo.mem.find;

private import llmo.mem;
import std.conv : to;

public:
/// Type for byte-pattern
struct Pattern_t{
	ushort _this; /// basic type
	alias _this this;
	pure nothrow @nogc @safe @property{
		/// Max value of type Pattern_t
		static ushort max(){
			return 0x100;
		}
	}
	/// Default ctor by value
	this(T)(T v){
		opAssign(v);
	}
	/// Operator =. assign value with check max
	auto ref opAssign(T)(T v){
		if (v > max)
			return _this = (v.to!ushort >> 8);
		return _this = v.to!ushort;
	}
	/// Operators like +=, -=, *=, /=
	auto ref opOpAssign(string op, T)(T v){
		return mixin("opAssign(cast(ushort)(_this"~op~"v.to!ushort))");
	}
	/// Cast this type to another types
	auto ref opCast(T)() const{
		return cast(T) _this;
	}
}

unittest{
	Pattern_t t = 0x101;
	assert(t == 0x01);
	t = 0x100;
	assert(t == 0x100);
	t *= 2;
	assert(t == 0x02);
	t += 4;
	assert(t == 0x06);
	t /= 2;
	assert(t == 0x03);
	t -= 1;
	assert(t == 0x02);
}

/**
 * Find address by pattern
 * pattern - array of ubytes, if value == 0x100 then it any ubyte
 *
 * Example:
 * Code: 'mov eax, 32; call 0x112233; ret'
 * Bytes: b820000000e829221100c3
 * Pattern: [0xb8, 0x20, 0x00, 0x00, 0x00, 0xe8, 0x100, 0x100, 0x100, 0x100, 0xc3]
 * Result: address to memory with code 'mov eax, 32; call ANY; ret'
 */
size_t findMemory(size_t addr, ssize_t length, Pattern_t[] pattern, MemorySafe safe = MemorySafe.valid){
	return cast(size_t)findMemory(cast(void*)addr, length, pattern, safe);
}
/**
 * Find address by pattern
 * pattern - array of ubytes, if value == 0x100 then it any ubyte
 *
 * Example:
 * Code: 'mov eax, 32; call 0x112233; ret'
 * Bytes: b820000000e829221100c3
 * Pattern: [0xb8, 0x20, 0x00, 0x00, 0x00, 0xe8, 0x100, 0x100, 0x100, 0x100, 0xc3]
 * Result: address to memory with code 'mov eax, 32; call ANY; ret'
 */
size_t findMemory(size_t addr, ssize_t length, ushort[] pattern, MemorySafe safe = MemorySafe.valid){
	return cast(size_t)findMemory(cast(void*)addr, length, pattern, safe);
}
/**
 * Find address by pattern
 * pattern - array of ubytes, if value == 0x100 then it any ubyte
 *
 * Example:
 * Code: 'mov eax, 32; call 0x112233; ret'
 * Bytes: b820000000e829221100c3
 * Pattern: [0xb8, 0x20, 0x00, 0x00, 0x00, 0xe8, 0x100, 0x100, 0x100, 0x100, 0xc3]
 * Result: address to memory with code 'mov eax, 32; call ANY; ret'
 */
void* findMemory(void* addr, ssize_t length, ushort[] pattern, MemorySafe safe = MemorySafe.valid){
	return findMemory(cast(void*)addr, length, pattern.to!(Pattern_t[]), safe);
}
/**
 * Find address by pattern
 * pattern - array of ubytes, if value == 0x100 then it any ubyte
 *
 * Example:
 * Code: 'mov eax, 32; call 0x112233; ret'
 * Bytes: b820000000e829221100c3
 * Pattern: [0xb8, 0x20, 0x00, 0x00, 0x00, 0xe8, 0x100, 0x100, 0x100, 0x100, 0xc3]
 * Result: address to memory with code 'mov eax, 32; call ANY; ret'
 */
void* findMemory(void* addr, ssize_t length, Pattern_t[] pattern, MemorySafe safe = MemorySafe.valid){
	ubyte[] buf;
	length -= pattern.length;
	for(ubyte* it = cast(ubyte*)addr; it < cast(ubyte*)(cast(size_t)addr+length); it++){
		buf = readMemory(cast(void*)it, pattern.length, safe);
		for(int i = 0; i < pattern.length; ++i){
			if (buf[i] != cast(ubyte)pattern[i] && pattern[i] != Pattern_t.max)
				break;
			if (i + 1 == pattern.length)
				return cast(void*)it;
		}
	}
	return null;
}

unittest{
	ubyte[] arr = [0x01, 0x02, 0x03, 0x04, 0x05];
	auto find = findMemory(cast(void*)arr.ptr, arr.length, [0x02, 0x100, 0x04]);
	assert((cast(ubyte*)find)[1] == 0x03);
}