module llmo.mem.cmp;

private import llmo.mem;
import core.stdc.string;

public:
/// Compare data from address with array
int cmpMemory(size_t addr, ubyte[] arr, MemorySafe safe = MemorySafe.valid){
	return cmpMemory(cast(void*)addr, arr, safe);
}
/// Compare data from address with array
int cmpMemory(void* addr, ubyte[] arr, MemorySafe safe = MemorySafe.valid){
	ubyte* _addr = cast(ubyte*)addr;
	ubyte[] _arr = arr;
	ubyte[] buf;
	while (true){
		if (_arr.length > 4096){
			buf = readMemory(cast(void*)_addr, 4096, safe);
			if (!buf.length)
				return 0;
			if (memcmp(buf.ptr, _arr.ptr, 4096))
				return 0;
			_addr += 4096;
			_arr = _arr[4096..$];
		} else {
			buf = readMemory(cast(void*)_addr, _arr.length, safe);
			if (!buf.length)
				return 0;
			if (memcmp(buf.ptr, _arr.ptr, _arr.length))
				return 0;
			break;
		}
	}
	return 1;
}