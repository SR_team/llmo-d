module llmo.mem.write;

private import llmo.mem;
import core.stdc.string;

public:
/// Write value to address
bool writeMemory(T)(size_t addr, T value, MemorySafe safe = MemorySafe.valid){
	return writeMemory(cast(T*)addr, value, safe);
}
/// Write value to address
bool writeMemory(T)(T* addr, T value, MemorySafe safe = MemorySafe.valid){
	if (safe == MemorySafe.valid || safe == MemorySafe.safe)
		if (!testMemoryAccess(addr))
			return false;
	uint prot;
	if (safe == MemorySafe.prot || safe == MemorySafe.safe){
		prot = getMemoryProtection(addr);
		setMemoryProtection(addr, T.sizeof, 0x40);
	}
	*addr = value;
	if (safe == MemorySafe.prot || safe == MemorySafe.safe)
		setMemoryProtection(addr, T.sizeof, prot);
	return true;
}

/// Write array to address
bool writeMemory(size_t addr, ubyte[] arr, MemorySafe safe = MemorySafe.valid){
	return writeMemory(cast(void*)addr, arr, safe);
}
/// Write array to address
bool writeMemory(void* addr, ubyte[] arr, MemorySafe safe = MemorySafe.valid){
	if (safe == MemorySafe.valid || safe == MemorySafe.safe)
		if (!testMemoryAccess(addr))
			return false;
	uint prot;
	if (safe == MemorySafe.prot || safe == MemorySafe.safe){
		prot = getMemoryProtection(addr);
		setMemoryProtection(addr, arr.length, 0x40);
	}
	memcpy(addr, arr.ptr, arr.length);
	if (safe == MemorySafe.prot || safe == MemorySafe.safe){
		setMemoryProtection(addr, arr.length, prot);
		version(Windows) FlushInstructionCache(GetCurrentProcess(), addr, arr.length);
	}
	return true;
}