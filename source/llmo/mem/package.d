module llmo.mem;

version (Windows) package import core.sys.windows.windows;
version (Posix) package import core.sys.posix.sys.mman;

public:
version(X86) alias ssize_t = int;
version(X86_64) alias ssize_t = long;
import llmo.mem.protect;
import llmo.mem.read;
import llmo.mem.write;
import llmo.mem.cmp;
import llmo.mem.set;
import llmo.mem.find;